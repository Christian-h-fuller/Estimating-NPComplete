import static java.util.function.Function.identity;
import java.util.stream.*;
import java.nio.file.*;
import java.util.*;
import java.io.*;

public class Main {
	static HashMap<Integer, HashSet<Integer>> nodes;
	static HashMap<Integer, Integer> nodeColors;
	static HashSet<Integer> colors;
	static int maxColor;

	public static void main(String args[]) {
		Scanner in = new Scanner(System.in);
		nodes = new HashMap<>();
		colors = new HashSet<>();
		nodeColors = new HashMap<>();
		maxColor = -1;
		System.out.println("Enter path to graph data");
		String path = in.nextLine();
		populate(path);
		color();
		System.out.println("Min coloring: " + (colors.size()));

	}

	public static void populate(String path) {
		try {
			Stream<String> fileStream = Files.lines(Paths.get(path));
			var count = 0;
			for (String i: fileStream.toArray(String[]::new)) {
				String[] temp = i.split(",");
				
				/*Doesn't do anything on the first iteration because all the 
				files have a header line of node_1,node_2 or something similar*/
				if (count != 0) {
					var nodeNum = Integer.parseInt(temp[0]);
					var neighNum = Integer.parseInt(temp[1]);

					//Building the graph, this bit is buidling and edge from nodeNum->neighNum
					if (nodes.containsKey(nodeNum)) {
						nodeColors.put(neighNum, -1);
						nodes.get(nodeNum).add(neighNum);
					} else {
						HashSet<Integer> newSet = new HashSet<>();
						newSet.add(neighNum);
						nodes.put(nodeNum, newSet);
						nodeColors.put(nodeNum, -1);
					}

					//Edge from neighNum->nodeNum
					if (nodes.containsKey(neighNum)) {
						nodeColors.put(nodeNum, -1);
						nodes.get(neighNum).add(nodeNum);
					} else {
						HashSet<Integer> newSet = new HashSet<>();
						newSet.add(nodeNum);
						nodes.put(neighNum, newSet);
						nodeColors.put(neighNum, -1);
					}
				}
				count++;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void color() {
		//loops through every entry in the map
		for (Map.Entry<Integer, HashSet<Integer>> i: nodes.entrySet()) {
			int node = i.getKey();
			HashSet<Integer> curNeighs = i.getValue();
			HashSet<Integer> colorSet = new HashSet<>(); //set of colors of the neighbors
			boolean newColor = true;

			//populating the color set of neighbors
			for (Integer j: curNeighs) {
				colorSet.add(nodeColors.get(j));
			}

			/*looking at all the colors that have been used so far to see if there is one that 
			this node can be colored without interfering with the neighbor's colors*/
			for (Integer j: colors) {
				if (!colorSet.contains(j)) {
					nodeColors.put(node, j);
					newColor = false;
					break;
				}
			}

			//adding a new color if necessary
			if (newColor) {
				maxColor++;
				colors.add(maxColor);
				nodeColors.put(node, maxColor);
			}

			System.out.println(nodeColors.get(node));
		}
	}
}
