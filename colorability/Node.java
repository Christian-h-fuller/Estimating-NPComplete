import java.util.*;

public class Node {
	private int color;
	private int num;
	private HashSet<Node> neighbors;

	public Node(int num) {
		this.num = num;
		neighbors = new HashSet<>();
		color = 0;
	}

	public void addNeighbor(Node n) {
		neighbors.add(n);
	}

	public void setColor(int color) {
  		this.color = color;
	}

	public HashSet<Node> getNeighbors() {
		return neighbors;
	}

	public boolean isNeighbor(Node n) {
		return neighbors.contains(n);
	}

	public int getColor() {
		return color;
	}

	@Override public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		Node node = (Node) o;
		return num == node.num;
	}

	@Override public int hashCode() {

		return Objects.hash(num);
	}

	@Override public String toString() {
		return ("Node: " + num + " Num neighbors: " + neighbors.size() + " Color: " + color + "\n");
	}
}
